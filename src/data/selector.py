import click
import pandas as pd


@click.command()
@click.option("--input_path", "-i", required=True, help="Input file path", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Output file path", type=click.Path())
def column_selector(input_path: str, output_path: str):
    """
    Selects columns from a CSV file and saves the result to a new CSV file.

    :param input_path:
    :param output_path:
    :return:
    """
    df = pd.read_csv(input_path)
    selected_columns = df[[
        "PassengerId",
        "HomePlanet",
        "CryoSleep",
        "Cabin",
        "Destination",
        "Age",
        "VIP",
        "RoomService",
        "FoodCourt",
        "ShoppingMall",
        "Spa",
        "VRDeck",
        "Transported"
    ]]

    selected_columns.columns = [
        "PASSENGER_ID",
        "HOME_PLANET",
        "CRYO_SLEEP",
        "CABIN",
        "DESTINATION",
        "AGE",
        "VIP",
        "ROOM_SERVICE",
        "FOOD_COURT",
        "SHOPPING_MALL",
        "SPA",
        "VR_DECK",
        "TRANSPORTED"
    ]

    selected_columns.to_csv(output_path, index=False)


if __name__ == '__main__':
    column_selector()
