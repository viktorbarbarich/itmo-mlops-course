import click
import pandas as pd


@click.command()
@click.option("--input_path", "-i", required=True, help="Input file path", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Output file path", type=click.Path())
def binner(input_path: str, output_path: str):
    """
    Bins a continuous variable into discrete intervals and saves the result to a new CSV file.

    :param input_path:
    :param output_path:
    :return:
    """
    df = pd.read_csv(input_path)
    df["HOME_PLANET"] = df["HOME_PLANET"].fillna("Unknown")
    df["CRYO_SLEEP"] = df["CRYO_SLEEP"].astype(str).fillna("Unknown")
    df["CABIN"] = df["CABIN"].fillna("0/0/0")  # Unknown value
    df["DESTINATION"] = df["DESTINATION"].fillna("TRAPPIST-1e")  # Mode
    df["AGE"] = df["AGE"].fillna(24)  # Mode
    df["VIP"] = df["VIP"].astype(str).fillna("Unknown")

    df["ROOM_SERVICE"] = df["ROOM_SERVICE"].fillna(df["ROOM_SERVICE"].mean().round())
    df["FOOD_COURT"] = df["FOOD_COURT"].fillna(df["FOOD_COURT"].mean().round())
    df["SHOPPING_MALL"] = df["SHOPPING_MALL"].fillna(df["SHOPPING_MALL"].mean().round())
    df["SPA"] = df["SPA"].fillna(df["SPA"].mean().round())
    df["VR_DECK"] = df["VR_DECK"].fillna(df["VR_DECK"].mean().round())

    df.to_csv(output_path, index=False)


if __name__ == '__main__':
    binner()
