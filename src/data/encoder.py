import click
import pandas as pd
from category_encoders.target_encoder import TargetEncoder


@click.command()
@click.option("--input_path", "-i", required=True, help="Input file path", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Output file path", type=click.Path())
def encoder_features(input_path: str, output_path: str):
    """
    Encodes categorical variables and saves the result to a new CSV file.

    :param input_path:
    :param output_path:
    :return:
    """
    df = pd.read_csv(input_path)
    ids = df["PASSENGER_ID"]

    df["CABIN_0"] = df["CABIN"].str.split("/").str[0]
    # df["CABIN_1"] = df["CABIN"].str.split("/").str[1]
    df["CABIN_2"] = df["CABIN"].str.split("/").str[2]
    df = df.drop(columns=["CABIN"])

    encoder = TargetEncoder()
    df_encoded = encoder.fit_transform(df.drop(columns=["TRANSPORTED", "PASSENGER_ID"]), df["TRANSPORTED"] * 1)
    df_encoded["TRANSPORTED"] = df["TRANSPORTED"] * 1

    df_encoded["PASSENGER_ID"] = ids
    df_encoded.to_csv(output_path, index=False)


if __name__ == '__main__':
    encoder_features()
