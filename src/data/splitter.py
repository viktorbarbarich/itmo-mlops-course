import click
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.option("--input_path", "-i", required=True, help="Input file path", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Output file path", type=click.Path())
def splitter(input_path: str, output_path: str):
    """
    Splits a dataset into training and testing sets and saves the result to new CSV files.

    :param input_path:
    :param output_path:
    :return:
    """
    df = pd.read_csv(input_path)
    train, test = train_test_split(df, test_size=0.2, random_state=42)

    train.to_csv(output_path.replace(".csv", "_train.csv"), index=False)
    test.to_csv(output_path.replace(".csv", "_test.csv"), index=False)


if __name__ == '__main__':
    splitter()
