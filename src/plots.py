import os

import click
import pandas as pd
import plotly.express as px
from loguru import logger
from sklearn.metrics import confusion_matrix

RANDOM_STATE = 21

# visual settings
vs = {
    "width": 800,
    "height": 500,
    "scale": 2
}


@click.command()
@click.option("--result_predict", "-r", required=True, type=click.Path())
@click.option("--test_file", "-i", required=True, type=click.Path())
@click.option("--output_path", "-o", required=True, type=click.Path())
def draw_plots(result_predict: str, test_file: str, output_path: str):
    logger.info("Drawing plots")

    data = pd.read_csv(test_file)
    predict = pd.read_csv(result_predict)

    cm = confusion_matrix(data["TRANSPORTED"], predict["TRANSPORTED_PREDICT"])
    cm_fig = px.imshow(cm, labels=dict(x="Predicted", y="True", color="Count"), text_auto=True,
                       title="<b>Confusion matrix")
    cm_fig.update_layout(
        title_font_size=20,
        xaxis=dict(title='Predicted Label', tickmode='array', tickvals=[0, 1], ticktext=['False', 'True']),
        yaxis=dict(title='True Label', tickmode='array', tickvals=[0, 1], ticktext=['False', 'True']),
        coloraxis_colorbar=dict(title='Count'),
        font=dict(size=14)
    )
    cm_fig.write_image(os.path.join(output_path, "confusion_matrix.png"), **vs)


if __name__ == "__main__":
    draw_plots()
