import click
import pandas as pd
from catboost import CatBoostClassifier

from loguru import logger


@click.command()
@click.option("--test_dataset", "-i", required=True, help="Test dataset path", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Result path", type=click.Path())
@click.option("--model_path", "-m", required=True, help="Model path", type=click.Path())
def test_predict(test_dataset: str, output_path: str, model_path: str):
    """

    :param test_dataset:
    :param output_path:
    :param model_path:
    :return:
    """
    model: CatBoostClassifier = CatBoostClassifier()
    model.load_model(model_path)
    logger.success("Model uploaded")

    data = pd.read_csv(test_dataset)
    logger.success("Data read")

    predict: pd.DataFrame = pd.DataFrame(model.predict(data.drop(columns=["PASSENGER_ID"])),
                                         columns=["TRANSPORTED_PREDICT"])
    predict["PASSENGER_ID"] = data["PASSENGER_ID"]
    logger.success("Predict done")

    predict.to_csv(output_path, index=False)


if __name__ == "__main__":
    test_predict()
