import click
import mlflow
import pandas as pd
from catboost import CatBoostClassifier
from dotenv import load_dotenv
from loguru import logger
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split

load_dotenv()

RANDOM_STATE = 21


@click.command()
@click.option("--train_dataset", "-i", required=True, help="Train dataset path", type=click.Path())
@click.option("--output_path", "-o", required=True, help="Output trained catboost model path", type=click.Path())
def main(train_dataset: str, output_path: str):
    logger.info("Training models")

    mlflow.set_experiment("MODEL TRAINING")

    data = pd.read_csv(train_dataset)
    data = data.drop(columns=["PASSENGER_ID"])

    x_train, x_valid, y_train, y_valid = train_test_split(
        data.drop(columns=["TRANSPORTED"]), data["TRANSPORTED"],
        stratify=data["TRANSPORTED"], test_size=0.33, random_state=RANDOM_STATE
    )

    params = {
        "iterations": 1000,
        "learning_rate": 0.1,
        "depth": 6,
        "l2_leaf_reg": 3,
        "border_count": 32,
        "thread_count": 4,
        "random_seed": RANDOM_STATE,
        "verbose": False,
        "custom_metric": "AUC",
    }
    hash_params = abs(hash(str(params))) % 1000000

    with mlflow.start_run(run_name=f"CatBoost-{hash_params}"):
        mlflow.log_params(params)

        model: CatBoostClassifier = CatBoostClassifier(**params)
        model.fit(x_train, y_train, eval_set=(x_valid, y_valid), early_stopping_rounds=5, use_best_model=True)

        roc_auc = roc_auc_score(y_valid, model.predict(x_valid))
        roc_auc_train = roc_auc_score(y_train, model.predict(x_train))

        mlflow.log_metric("roc_auc", roc_auc)
        mlflow.log_metric("roc_auc_train", roc_auc_train)

        signature = mlflow.models.infer_signature(x_train, model.predict(x_train))

        mlflow.catboost.log_model(model, f"model-{roc_auc:.4f}-roc_auc-{hash_params}", signature=signature)

        logger.success(f"End training. ROC_AUC VALIDATION: {roc_auc: 0.4%}")
        logger.success(f"End training. ROC_AUC TRAINING: {roc_auc_train: 0.4%}")

        logger.success("Modeling training complete.")

        model.save_model(output_path)
        logger.success(f"Model saved to {output_path}")
        mlflow.log_artifact(output_path)


if __name__ == "__main__":
    main()
